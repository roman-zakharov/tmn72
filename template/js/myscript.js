$(document).ready(function(){
	$("a.parent").on('click',function(){
		
		$('a.parent').removeClass("left-menu-active-link");
		$(this).addClass("left-menu-active-link");
		
		$('a.parent').siblings("ul").hide();
		$(this).siblings("ul").show();
		
		return false;
	});
});